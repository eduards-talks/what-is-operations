<!-- .slide: data-state="no-toc-progress" --> <!-- don't show toc progress bar on this slide -->

## What is Operations
<!-- .element: class="no-toc-progress" --> <!-- slide not in toc progress bar -->

----

## The first slide

_Operations in itself is not inherently dangerous. But to an even greater degree than the sea, it is terribly unforgiving of any carelessness, incapacity, neglect or ignorance._

----  ----

## The last slide
